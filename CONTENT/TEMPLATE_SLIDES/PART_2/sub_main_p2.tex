\section{Branch-and-bound algorithm for non-OR community}
\input{CONTENT/DECORUM/current_toc}
\begin{frame}
  \frametitle{Branch-and-bound algorithm \citep{land_automatic_1960}}
  \begin{block}{OR's Mantra}
      When an exhaustive search of all feasible solutions is inconceivable, take a break and implement a branch-and-bound.
  \end{block}
  \vspace*{0.5cm}
  \begin{exampleblock}{Main principle}
      \begin{itemize}
        \item Divide \& conquer approach;
        \item Use of bounds on the optimal value to avoid the exploration of some regions of the search space; for a minimization problem :
              $$\underline{z} \leq z^{\ast} \leq \overline{z}$$
              \vspace*{-0.5cm}
              \begin{description}
                  \item[The dual bound $\underline{z} \in \R$: ] a \textbf{lower bound} computed by a \textbf{relaxation}, often \textbf{easier to compute}; %or the \textbf{dual problem evaluation};
                  \item[The primal bound $\overline{z}\in \R$: ] an \textbf{upper bound} given by any \textbf{feasible} solution.
              \end{description}
        \item Build an \textbf{exploration tree} by repeating two steps: the \textbf{Branch} \& the \textbf{Bound} operations.
      \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  Let us consider:
  $$ (\mathcal{P}) : \min\left\{f(\xvec)=\xone -2\xtwo\ms|\ms-4\xone+6\xtwo\leq9;\ms\xone+\xtwo\leq4;\ms\xvec\in\mathbb{Z}_{+}^{2}\right\}$$
  \begin{figure}[t]
      \scalebox{0.65}{
          \input{CONTENT/PART_2/PLAN/plan_0}
      }
  \end{figure}
  \textbf{Our objective : } find $\xExUB := \Argmin(\mathcal{P})$
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_0}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_0}
        }
    \end{figure}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ (\ProbA) : \min\left\{f(\xvec)=\xone -2\xtwo\ms|\ms-4\xone+6\xtwo\leq9;\ms\xone+\xtwo\leq4;\ms \textcolor{ao}{\xvec\in\mathbb{R}_{+}^{2}}\right\}$$
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_1_relax}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_0}
        }
    \end{figure}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ (\ProbA) : \min\left\{f(\xvec)=\xone -2\xtwo\ms|\ms-4\xone+6\xtwo\leq9;\ms\xone+\xtwo\leq4;\ms \textcolor{ao}{\xvec\in\mathbb{R}_{+}^{2}}\right\}$$
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_1_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_1_solved}
        }
    \end{figure}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  \textbf{Branch : } Use the optimum value to define inequalities and create a $b$-partition of the search space (exhaustive but not necessarily mutually exclusive); \newline
  \eg Most infeasible branching $\Leftrightarrow$ most fractional component
  \begin{figure}[t]
      \centering
      \scalebox{1}{
          \input{CONTENT/PART_2/TREE/tree_ex_br}
      }
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbB := \ProbA \cap \{\xtwo \geq  3\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_2}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_2}
        }
    \end{figure}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbB := \ProbA \cap \{\xtwo \geq  3\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_2}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_2_bounded}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  \begin{description}
    \item[$\quad\quad$] \textcolor{cadmiumred}{$\Rightarrow$} Pruning $\ProbB$ by \textbf{nonfeasibility} !
  \end{description}

\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbC := \ProbA \cap \{\xtwo \leq  2\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_3}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_3}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  %$\quad\quad$\textcolor{cadmiumred}{$\Rightarrow$} Pruning $\ProbB$ by \textbf{nonfeasibility} !
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbC := \ProbA \cap \{\xtwo \leq  2\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_3_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_3_solved}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  \begin{description}
    \item[$\quad\quad$] \textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: \textbf{Branch} on $\ProbC$!
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbD := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \geq  1\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_4}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_4}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbD := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \geq  1\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_4_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_4_solved}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  \begin{description}
    \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Prune $\ProbD$ by \textbf{optimality} \ie $\exists \xExASt\ms$ $\mathbb{Z}_{+}$-feasible $\st f(\xExASt) < \zExUB $;
    \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Update the global upper bound $\zExUB$
  \end{description}

  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbD := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \geq  1\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_4_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_4_updated}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  \begin{description}
    \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Prune $\ProbD$ by \textbf{optimality} \ie $\exists \xExASt\ms$ $\mathbb{Z}_{+}$-feasible $\st f(\xExASt) < \zExUB $;
    \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Update the global upper bound $\zExUB$
  \end{description}

  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbE := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \leq  0\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_5}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_5}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  % \begin{description}
  %   \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Prune $\ProbD$ by \textbf{optimality} \ie $\exists \xExASt\ms$ $\mathbb{Z}_{+}$-feasible $\st f(\xExASt) < \zExUB $;
  %   \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Update the global upper bound $\zExUB$
  % \end{description}

  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbE := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \leq  0\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_5_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_5_solved}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}
  % \begin{description}
  %   \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Prune $\ProbD$ by \textbf{optimality} \ie $\exists \xExASt\ms$ $\mathbb{Z}_{+}$-feasible $\st f(\xExASt) < \zExUB $;
  %   \item[$\quad\quad$] \textcolor{ao}{$\Rightarrow$} Update the global upper bound $\zExUB$
  % \end{description}

  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}

\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ \ProbE := \ProbA \cap \{\xtwo \leq  2\} \cap \{\xone \leq  0\} $$
  \vspace*{-0.35cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_5_relax_solved}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_5_bounded}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \vspace*{0.35cm}

  \begin{description}
    \item[$\quad\quad$] \textcolor{darkpastelgreen}{$\Rightarrow$} Prune $\ProbE$ by \textbf{dominance} \ie $\forall \xExASt, f(\xExASt) \geq f(\xExUB)$;
  \end{description}

  %$\quad\quad$\textcolor{battleshipgrey}{$\Rightarrow$} Nothing can be concluded: Branch on $\ProbC$!
\end{frame}


\begin{frame}
  \frametitle{Branch-and-bound algorithm by the example}
  $$ (\mathcal{P}) : \min\left\{f(\xvec)=\xone -2\xtwo\ms|\ms-4\xone+6\xtwo\leq9;\ms\xone+\xtwo\leq4;\ms\xvec\in\mathbb{Z}_{+}^{2}\right\}$$
  \vspace*{-0.5cm}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/PLAN/plan_6_final}
        }
    \end{figure}
  \end{column}
  \begin{column}{0.5\textwidth}  %%<--- here
    \begin{figure}[t]
        \scalebox{0.65}{
            \input{CONTENT/PART_2/TREE/tree_6_final}
        }
    \end{figure}
  \end{column}
  \end{columns}
  \begin{description}
    \item[$\quad\quad$] \textcolor{battleshipgrey}{$\Rightarrow$} \textbf{Founded} and \textbf{certified} optimality : $\vxh=(1,2)$,  $\zExUB=-3$.
  \end{description}

\end{frame}



\begin{frame}
  \frametitle{Branch-and-bound algorithm \citep{land_automatic_1960}}
  % \begin{exampleblock}{Optimality certification}
  %     \begin{description}
  %       \item[Branch operations:] explore the search (sub)spaces to find feasible solutions;
  %       \item[Bound operations:] prove $\vx=\Argmin_{\vx\in\Sall}(f(\vx))$ since there is \textbf{no nondominated} search subspace \ie $\listL = \emptyset$;
  %     \end{description}
  % \end{exampleblock}
  \textcolor{UBCblue}{\textbf{Computational complexity:}} $\mathcal{O}(Tb^{Q})$ with
  \begin{description}
      \item[$b$:] \textit{branching factor}
      \item[$T$:] a fixed bound on time needed to explore the search (sub)spaces;
      \item[$Q$:] the length of the longest path from the root to a leaf.
  \end{description}

  \begin{block}{How to tune this algorithm ?}
      \begin{description}
          \item[$\ms$Branching strategy:]\textcolor{UBCwhite}{?}
          \begin{itemize}
              \item Which value for the $b\in\mathbb{N}^{*}$ ?
              \item How to choose the branching variable ${\x}_{i}$ ?
          \end{itemize}
          \item[$\ms$Pruning rules:]\textcolor{UBCwhite}{?}
          \begin{itemize}
              \item How to compute the lower bounds ?
              \item Add other valid inequality, cutting planes, dominance rules, \dots
          \end{itemize}
          \item[$\ms$Search strategy:]\textcolor{UBCwhite}{?}
          \begin{itemize}
            \item How to explore the tree ? \eg DFS, BFS, \dots
          \end{itemize}
      \end{description}
  \end{block}
\end{frame}
