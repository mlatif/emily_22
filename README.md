# EmiLy 2022

Support de présentation :   
- Workshop [EmiLy](https://emilyworkshop.github.io/)
- Demi-Heure des Doctorant·e·s (DHD) - 25/10, 10h30

**Titre de la présentation:** Challenges and opportunities of three-photon tomographic reconstruction for the XEnon Medical Imaging System (XEMIS2)

- Page de correction: [ici](https://uncloud.univ-nantes.fr/index.php/s/W5KFF8rsFnYp3Qe)

## Mantras GIT

```
cd existing_repo
git remote add origin https://gitlab.com/mlatif/emily_22.git
git branch -M main
git push -uf origin main
```

*use [tiftex](https://gitlab.com/mlatif/tif-tex)*
